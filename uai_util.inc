<?php

/**
 * Constant declarations
 */
define('UAI_MODE_DISABLED'  , 0);
define('UAI_MODE_PAM'       , 1);
define('UAI_MODE_DRUPAL_PAM', 2);

define('UAI_UD_DISABLED', 0);
define('UAI_UD_ENABLED' , 1);

define('UAI_G2R_BLOCK',    0);
define('UAI_G2R_DISABLED', 1);
define('UAI_G2R_SINGLE',   2);
define('UAI_G2R_AUTO',     3);


/**
 * Convenient wrappers around variable_(get|set|del) functions
 */
function uai_var_del($var) {
  variable_del($var);
}

function uai_var_write($var, $value) {
  variable_set($var, $value);
}

// Wrapper arround variable_get(), to define default values only once
// and in only one place
function uai_var_read($var) {
  $defaults = array(
    'uai_mode'            => UAI_MODE_DISABLED,
    'uai_user_dialogs'    => UAI_UD_ENABLED,
    'uai_g2r_integration' => FALSE,
    'uai_g2r_mode'        => UAI_G2R_DISABLED,
    'uai_g2r_default_role' => '',
    'uai_group_role_maps' => array(),
    'uai_email_domain'    => 'example.com',
    'uai_glist'           => array(),
    'uai_glist_refresh_progress' => 0
  );

  if($var == '_list_') {
    return array_keys($defaults);
  } else {
    return variable_get($var, $defaults{$var});
  }
}


/**
 * Return an array with Drupal roles names, excluding "Anonymous" and "Autenticated User"
 */
function uai_get_roles() {
  $roles = array();
  foreach(user_roles() as $id => $role) {
    if($id > 2) {
      $roles[] = $role;
    }
  }
  return $roles;
}


/**
 * Check if a given role exists on Drupal
 */
function uai_role_exists($role_name){
  return user_role_load_by_name($role_name) === FALSE ? FALSE : TRUE;
}


/**
 * Create a new role (if it does not exist).
 */
function uai_create_role($role_name){
  if(! uai_role_exists($role_name)) {
    $role = new stdClass();
    $role->name = $role_name;
    if(user_role_save($role)) {
      return user_role_load_by_name($role_name);
    }
  }
  return FALSE;
}


/**
 * Returns the user's Unix group name
 */
function uai_get_unix_group($username) {
  if(! (function_exists('posix_getpwnam') && function_exists('posix_getgrgid'))) {
    watchdog('uai_module', 'PHP Posix functions are not available.  Group to role operations will not be performed.', array(), WATCHDOG_ERROR);
    return FALSE;
  }

  if(! $user_data = posix_getpwnam($username)) {
    return FALSE;
  }

  if(! $group_data = posix_getgrgid($user_data['gid'])) {
    return FALSE;
  } else {
    return $group_data['name'];
  }
}


/**
 * Check if a given username was created or was logged on at least once by the UAI module
 */
function uai_is_uai_user($username) {
  $auth_module = user_get_authmaps($username);

  if(! is_array($auth_module)) {
    // $auth_module will be "0" for local users
    return FALSE;
  }

  return array_key_exists('UAI', $auth_module);
}


/**
 * Logs the user on and perform additional setup actions
 */
function uai_start_session($username) {
  $account = FALSE;

  if(! ($account = user_external_load($username))) {
    // Unable to load external user (account does not exists or is a native account)
    if($account = user_load_by_name($username)) {
      // User have an native account: set the authmap
      user_set_authmaps($account, array('authname_UAI' => $username));
    }
  }

  if(! $account) {
    // Account does not exists: create as an external user
    user_external_login_register($username, 'UAI');
    $account = user_external_load($username);
  }

  // Enforces account details
  uai_account_setup($account);

  return $account;
}


/**
 * Set up account details
 */
function uai_account_setup($account) {
  $username = $account->name;

  $details = array(
    'mail' => $username . '@' . uai_var_read('uai_email_domain'),
    'timezone' => variable_get('date_default_timezone', 'UTC')
  );
  
  user_save($account, $details);
}


/**
 * Remove group-to-role map entries referring to nonexistent Drupal roles
 */
function uai_validate_role_map($target_group = FALSE) {
  $group_role_maps = uai_var_read('uai_group_role_maps');

  $update = FALSE;
  foreach($group_role_maps as $group => $roles) {
    if($target_group && ($target_group != $group)) {
      continue;
    }
    $expired_roles = array();
    foreach($roles as $role) {
      if(! uai_role_exists($role)) {
        $update = TRUE;
        $expired_roles[] = $role;
      }
    }
    if(! empty($expired_roles)) {
      $group_role_maps[$group] = array_diff($roles, $expired_roles);
    }
  }

  if($update) {
    uai_var_write('uai_group_role_maps', $group_role_maps);
  }
}


/**
 * Return an array with the roles mapped by the given Unix group
 * This function takes extra time as it validates the group list
 * before returning it
 */
function uai_get_roles_by_group($group = FALSE) {
  uai_validate_role_map($group);
  $group_role_maps = uai_var_read('uai_group_role_maps');
  $result = FALSE;
  if($group) {
    if(isset($group_role_maps[$group])) {
      $result = $group_role_maps[$group];
    }
  } else {
    $result = $group_role_maps;
  }
  return $result;
}


/**
 * Add an $group -> $role entry on the Group-to-Role map
 */
function uai_add_g2r_entry($group, $role) {
  $group_role_maps = uai_var_read('uai_group_role_maps');
  if(isset($group_role_maps[$group])) {
    // Group entry already exists: just add the role map to it
    if(! in_array($role, $group_role_maps[$group])) {
      $group_role_maps[$group][] = $role;
      uai_var_write('uai_group_role_maps', $group_role_maps);
    }
  } else {
    // Create a new Group entry
    $group_role_maps[$group] = array($role);
    uai_var_write('uai_group_role_maps', $group_role_maps);
  }
}


/**
 * Replace user's roles with the ones on the "$role_names" parameter.
 * Create roles as needed
 */
function uai_set_account_roles($account, $role_names) {
    $details = array();
    $user_roles = array();
    foreach($role_names as $role_name) {
      if(! ($role_obj = user_role_load_by_name($role_name))) {
        $role_obj = uai_create_role($role_name);
      }
      $user_roles[$role_obj->rid] = $role_name;
    }

    $details['roles'] = $user_roles;
    user_save($account, $details);
}


/**
 * Authenticate users via PAM
 */
function uai_pam_auth($username, $password) {
  $error = '';

  if(! function_exists('pam_auth')) {
    watchdog('uai_module', 'PHP PAM Authentication function is not available.  Users will not be able to authenticate using their Unix account.', array(), WATCHDOG_ERROR);
    return FALSE;
  }
  
  if(uai_var_read('uai_mode') == UAI_MODE_DISABLED) {
    return FALSE;
  }

  if(pam_auth($username, $password, $error)) {
    return TRUE;
  } else {
    watchdog('uai_module', 'PAM Authentication failed for user %user: %error', array('%user' => $username, '%error' => $error), WATCHDOG_WARNING);
    return FALSE;
  }
}
